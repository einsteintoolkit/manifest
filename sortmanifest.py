#!/usr/bin/env python3

import re
import sys

NotFound = "zzz"

# Read in the file
lines = list()
if len(sys.argv) == 1:
    fname = "einsteintoolkit.th"
else:
    fname = sys.argv[1]
with open(fname, "r") as fd:
    for line in fd.readlines():
        lines.append(line)

# Divide the file into sections of text delimited by blank lines
sections = list()
section = list()
section.append(lines[0])
for i in range(1,len(lines)):
    if lines[i-1].strip() == "" and lines[i].strip() != "":
        sections.append(section)
        section = list()
    section.append(lines[i])
sections.append(section)

def thornSortKey(line):
    g = re.match(r'(#DISABLED\s+|#\s*|)(\w+)/(\w+)\s*(#.*|)', line)
    if g:
        return g.group(3).lower().replace("_","")
    else:
        return NotFound

# Sort a section of text
def sortSection(section):
    i = 0
    while i < len(section):
        # Look for the first line in the section
        # for which a sort key is found. That is
        # the beginning of the list of thorns.
        if thornSortKey(section[i]) != NotFound:
            break
        i += 1
    # section[0:i] is now comments and header information
    # section[i:] is the thorn list
    return section[0:i] + sorted(section[i:], key=thornSortKey)

def sectionSortKey(section):
    # Assumes all sections contain a single arrangement
    arrangementnames = set()
    for line in section:
        g = re.match(r'^(#DISABLED\s+|)(\w+)/(\w)', line)
        if g:
            key = g.group(2)
            arrangementnames.add(key.lower())
    if len(arrangementnames) == 1:
        return key
    else:
        return NotFound

# Leave everything alone from the top to coredoc
i = 0
while i < len(sections):
    key = sectionSortKey(sections[i])
    if key == "coredoc":
        i += 1
        break
    i += 1

# Print the sorted list
for section in sections[0:i]+sorted(sections[i:], key=sectionSortKey):
    section = sortSection(section)
    for line in section:
        print(line,end='')
